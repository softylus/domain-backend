const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
var FormData = require("form-data");
const axios = require("axios");
const qs = require("qs");
const app = express();
let httpsProxyAgent = require("https-proxy-agent");
const schedule = require("node-schedule");
const fs = require("fs");
const fs2 = require("fs").promises;

const agent = new httpsProxyAgent(
	`http://aashra02:softprox@23.106.192.154:29842`
);
var cors = require("cors");
app.use(cors());

const str = ["com", "net", "info", "store"];
const stripe = require("stripe")(
	"sk_test_51FLPN8DIguYJOS5S5LSq5zCIOUI4t7DXltagyX0q64oUI2tfqE8QdbcoU9zi5o0KH771EZma0NXfMpk1s5RslPhL006QiDES35"
);

app.use(express.static("."));
app.use(express.json());

const calculateOrderAmount = (items) => {
	return 1400;
};
app.post("/create-payment-intent", async (req, res) => {
	const { items } = req.body;
	// Create a PaymentIntent with the order amount and currency
	const paymentIntent = await stripe.paymentIntents.create({
		amount: calculateOrderAmount(items),
		currency: "usd",
	});
	res.send({
		clientSecret: paymentIntent.client_secret,
	});
});

var res = str.slice(str.indexOf(".") + 1, str.length);

// items.map((item) => {
// 	// arr[item] = result.pricing.item.register[1];
// 	console.log(item);
// });
// const s = ["com", "net", "info"];
// let arr = {
// 	com: result.pricing.com.register[1],
// 	net: result.pricing.net.register[1],
// 	info: result.pricing.info.register[1],
// 	store: result.pricing.store.register[1],
// };
// s.map((item) => {
// 	arr[item] = result.pricing.item.register[1];
// });

// console.log(arr[res]);
// });

const port = process.env.PORT || 3000;
// create application/json parser
const jsonParser = bodyParser.json();

app.post("/getProductPrice", jsonParser, async (req, res) => {
	try {
		var data = new FormData();
		data.append("username", "aseel");
		data.append("password", "7ef6736f48c4b3422c100c22a25c60b6");
		data.append("action", "GetProducts");
		data.append("currencyid", "1");
		data.append("responsetype", "json");
		var config = {
			method: "post",
			url: "https://client.hostylus.com/includes/api.php",
			headers: {
				"Access-Control-Allow-Origin": "*",
				...data.getHeaders(),
			},
			data: data,
		};
		axios(config)
			.then(function(response) {
				res.json({
					status: 200,
					totalNumber: response.data.totalresults,
					data: response.data.products.product,
				});
			})
			.catch(function(error) {
				console.log(error);
			});
	} catch (e) {
		console.log(e);
	}
});

schedule.scheduleJob("  0 0 * * *", () => {
	var data = new FormData();
	data.append("username", "aseel");
	data.append("password", "7ef6736f48c4b3422c100c22a25c60b6");
	data.append("action", "gettldpricing");
	data.append("currencyid", "1");
	data.append("responsetype", "json");
	var config = {
		method: "post",
		url: "https://client.hostylus.com/includes/api.php",
		headers: {
			"Access-Control-Allow-Origin": "*",
			...data.getHeaders(),
		},
		data: data,
	};

	axios(config)
		.then(function(response) {
			const arr = {};
			const items = response.data.pricing;
			for (var item in items) {
				if (items.hasOwnProperty(item)) {
					arr[item] = items[item].register[1];
				}
			}

			fs.writeFile("tldPrices.json", JSON.stringify(arr), function(err) {
				if (err) return console.log(err);
			});
		})

		.catch(function(error) {
			console.log(error);
		});
});

app.get("/getAva", jsonParser, async (req, res) => {
	try {
		let url = new URL(
			"https://domaincheck.httpapi.com/api/domains/available.json?"
		);
		var data = new FormData();
		url.searchParams.append("auth-userid", "1019188");
		url.searchParams.append("api-key", "A1Oq96WGjrgunKwDLoKLVvBylB4MmlD3");
		url.searchParams.append("domain-name", req.query.domainName);
		req.query.tlds.forEach((tld) => url.searchParams.append("tlds", tld));
		var config = {
			httpsAgent: agent,
			method: "get",
			url: url.toString(),
			headers: {
				"Access-Control-Allow-Origin": "*",
				...data.getHeaders(),
			},
			data: data,
		};
		// let url2 = new URL(
		// 	"https://domaincheck.httpapi.com/api/domains/premium/available.json?"
		// );
		// var data2 = new FormData();
		// url2.searchParams.append("auth-userid", "1019188");
		// url2.searchParams.append("api-key", "A1Oq96WGjrgunKwDLoKLVvBylB4MmlD3");
		// url2.searchParams.append("key-word", req.query.domainName);
		// req.query.tlds.forEach((tld) => url2.searchParams.append("tlds", tld));
		// url2.searchParams.append("price-high", "1000");
		// url2.searchParams.append("no-of-results", "10");
		// var configPremuim = {
		// 	httpsAgent: agent,
		// 	method: "get",
		// 	url: url2.toString(),
		// 	headers: {
		// 		"Access-Control-Allow-Origin": "*",
		// 		...data2.getHeaders(),
		// 	},
		// 	data: data2,
		// };
		let url3 = new URL(
			"https://test.httpapi.com/api/domains/v5/suggest-names.json?"
		);
		var data3 = new FormData();
		url3.searchParams.append("auth-userid", "1019188");
		url3.searchParams.append("api-key", "A1Oq96WGjrgunKwDLoKLVvBylB4MmlD3");
		url3.searchParams.append("keyword", req.query.domainName);
		url3.searchParams.append("no-of-results", "40");
		url3.searchParams.append("add-related", true);

		var configSuggest = {
			httpsAgent: agent,
			method: "get",
			url: url3.toString(),
			headers: {
				"Access-Control-Allow-Origin": "*",
				...data3.getHeaders(),
			},
			data: data3,
		};

		//domain availability
		const requestOne = axios(config);

		//get premium domains
		// const requestTwo = axios(configPremuim);

		//get suggestions
		const requestThree = axios(configSuggest);

		axios
			.all([requestOne, requestThree])
			.then(
				axios.spread(async (...response) => {
					// take suggestions and insert prices for them

					const Suggestions = Object.getOwnPropertyNames(response[1].data);
					var SuggestionsPrices = {};

					for (var item in Suggestions) {
						if (Suggestions.hasOwnProperty(item)) {
							const tld = Suggestions[item].slice(
								Suggestions[item].indexOf(".") + 1,
								Suggestions[item].length
							);
							const ele = Suggestions[item];

							const data = await fs2.readFile("./tldPrices.json", "utf-8");
							let result = JSON.parse(data);
							console.log(data);
							SuggestionsPrices[ele] = result[tld];
						}
					}

					let resp = [response[0].data, SuggestionsPrices];
					res.json({
						status: 200,
						data: resp,
					});
				})
			)
			.catch(function(error) {
				console.log(error);
			});
	} catch (e) {
		console.log(e);
	}
});

app.listen(port, () => {
	console.log("Hello World I run on PORT " + port);
});
